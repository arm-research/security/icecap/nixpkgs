{ lib, stdenv, fetchFromGitHub, autoreconfHook, pkg-config, which
, libmysqlclient, libaio, buildPackages
}:

stdenv.mkDerivation rec {
  pname = "sysbench";
  version = "1.0.20";

  nativeBuildInputs = [ autoreconfHook pkg-config which ];
  buildInputs = [ libaio ];
  depsBuildBuild = [ buildPackages.stdenv.cc ];

  src = fetchFromGitHub {
    owner = "akopytov";
    repo = pname;
    rev = version;
    sha256 = "1sanvl2a52ff4shj62nw395zzgdgywplqvwip74ky8q7s6qjf5qy";
  };

  postPatch = ''
    substituteInPlace third_party/concurrency_kit/ck/configure --replace 'COMPILER=`./.1 2> /dev/null`' 'COMPILER=gcc'
  '';

  preConfigure = ''
    d=./hack-bin
    mkdir $d
    ln -s $(which $PKG_CONFIG) $d/pkg-config
    export PATH=$PATH:$(realpath $d)
  '';

  configureFlags = [ "--without-mysql" ];

  CK_CONFIGURE_FLAGS = "--platform=arm64";

  CROSS = stdenv.cc.targetPrefix;

  enableParallelBuilding = true;

  meta = {
    description = "Modular, cross-platform and multi-threaded benchmark tool";
    homepage = "https://github.com/akopytov/sysbench";
    license = lib.licenses.gpl2;
    platforms = lib.platforms.linux;
  };
}
