{ lib, stdenv, buildPackages, fetchurl, libtool, gettext }:

stdenv.mkDerivation rec {
  name = "cpufrequtils-008";

  src = fetchurl {
    url = "http://ftp.be.debian.org/pub/linux/utils/kernel/cpufreq/${name}.tar.gz";
    sha256 = "127i38d4w1hv2dzdy756gmbhq25q3k34nqb2s0xlhsfhhdqs0lq0";
  };

  patches = [
    # I am not 100% sure that this is ok, but it breaks repeatable builds.
    ./remove-pot-creation-date.patch
  ];

  depsBuildBuild = [
    buildPackages.stdenv.cc
  ];

  patchPhase = ''
    sed -e "s@= /usr/bin/@= @g" \
      -e "s@/usr/@$out/@" \
      -i Makefile
  '' + (with stdenv; lib.optionalString (buildPlatform != hostPlatform) ''
    sed -i 's,CROSS = .*,CROSS = ${cc.targetPrefix},' Makefile
    sed -i 's,utils: cpufreq-info cpufreq-set cpufreq-aperf,utils: cpufreq-info cpufreq-set,' Makefile
    sed -i '/$(INSTALL_PROGRAM) cpufreq-aperf/d' Makefile
  '');

  nativeBuildInputs = [ libtool gettext ];
  buildInputs = [ stdenv.cc.libc.linuxHeaders ];

  meta = with lib; {
    description = "Tools to display or change the CPU governor settings";
    homepage = http://ftp.be.debian.org/pub/linux/utils/kernel/cpufreq/cpufrequtils.html;
    license = licenses.gpl2;
    platforms = platforms.linux;
  };
}
